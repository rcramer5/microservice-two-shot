from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import BinVO, Shoe
from .acls import get_photo


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "closet_name"]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["color", "manufacturer", "model_name", "picture_url", "bin", "id"]
    encoders = {
        "bin": BinVODetailEncoder()
    }


@require_http_methods(["GET", "POST",])
def api_list_shoe(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin ID"},
                status=400,
            )
        photo = get_photo(content["color"], content["manufacturer"])
        content.update(photo)
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_show_shoe(request, pk):
    count, _ = Shoe.objects.filter(id=pk).delete()
    return JsonResponse({"deleted": count > 0})