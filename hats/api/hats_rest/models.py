from django.db import models
# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length= 100)

class Hat(models.Model):
    fabric = models.CharField(max_length=30)
    style = models.CharField(max_length=30)
    color = models.CharField(max_length=30)
    picture_url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name= "hats",
        on_delete = models.CASCADE
    )


