import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList'
import ShoesList from "./ShoesList"
import HatForm from './HatForm';
import ShoeForm from './CreateShoe'

function App() {

  // if (props.hats === undefined) {
  //   return null;
  // }
  // if (props.shoes === undefined){
  //   return null;
  // }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path ="hats">
            <Route path="" element={<HatsList />} /> 
            <Route path= "new" element={<HatForm />} />
          </Route>
          <Route path="shoes">
            <Route path="" element={<ShoesList/>} />
            <Route path="new" element={<ShoeForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
