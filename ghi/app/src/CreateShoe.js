import React from 'react';

class ShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturer: "",
            model_name: "",
            color: "",
            bins: []
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(event) {
        const value = event.target.value;
        const key = event.target.name;
        const changeDict = {}
        changeDict[key] = value;
        this.setState(changeDict);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.bins

        const shoesUrl = "http://localhost:8080/api/shoes/";
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            },
        };
        const shoeResponse = await fetch(shoesUrl, fetchConfig);
        if (shoeResponse.ok){
            const newShoe = await shoeResponse.json();
            const cleared = {
                manufacturer: "",
                model_name: "",
                color: "",
                bin: "",
            };
            this.setState(cleared);
        }
    }

    async componentDidMount(){
        const binUrl = "http://localhost:8100/api/bins/";

        const binResponse = await fetch(binUrl)

        if (binResponse.ok){
            const binData = await binResponse.json()
            this.setState({bins: binData.bins})
        }
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    < div className="shadow p-4 mt-4">
                        <h1>Create a shoe</h1>
                        <form onSubmit={this.handleSubmit} id="create-shoe-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.manufacturer} placeholder="Manufacturer" required type="text" id="manufacturer" name="manufacturer" className='form-control' />
                                <label htmlFor="manufacturer">Manufacturer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.model_name} placeholder="Model Name" required type="text" id="model_name" name="model_name" className='form-control' />
                                <label htmlFor="model_name">Model Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.color} placeholder="Color" required type="text" id="color" name="color" className='form-control' />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} value={this.state.bin} required id="bin" name="bin" className='form-select'>
                                    <option value="">Choose a bin</option>
                                    {this.state.bins.map(bin => {
                                        return (
                                            <option key={bin.href} value={bin.href}>
                                                {bin.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default ShoeForm;