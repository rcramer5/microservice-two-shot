import React from 'react';

class HatForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      style: '',
      color: '',
      fabric: '',
      location: '',
      locations: []
    };
    this.handleStyleChange = this.handleStyleChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handleFabricChange = this.handleFabricChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.locations
    const locationUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      console.log(newLocation);

      const cleared = {
        style: '',
        fabric: '',
        color: '',
        location: '',
      };
      this.setState(cleared);
    }
  }

  handleStyleChange(event) {
    const value = event.target.value;
    this.setState({style: value})
  }

  handleFabricChange(event) {
    const value = event.target.value;
    this.setState({fabric: value})
  }

  handleColorChange(event) {
    const value = event.target.value;
    this.setState({color: value})
  }

  handleLocationChange(event) {
    const value = event.target.value;
    this.setState({location: value});
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({locations: data.locations});
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={this.handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input value={this.state.style} onChange={this.handleStyleChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                <label htmlFor="name">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.roomCount} onChange={this.handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="room_count">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.color} onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="city">Color</label>
              </div>
              <div className="mb-3">
                <select value={this.state.location} onChange={this.handleLocationChange} required name="location" id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {this.state.locations.map(location => {
                    return (
                      <option key={location.id} value={location.href}>
                        {location.closet_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default HatForm;