import React from 'react'
import {Component} from 'react'

class HatsList extends Component{
  constructor(props){
    super(props)
    this.state = {hats: []}
  }
  async componentDidMount()
  {
    const url = 'http://localhost:8090/api/hats/'
    const res = await fetch(url)
    const hatsJSON = await res.json()
    this.setState({hats: hatsJSON.hats.slice(0,100)})
  }
  async delete(id)
  {
    const deleteHatURL = `http://localhost:8090/api/hats/${id}`
    const fetchConfig = {
      method: "delete"
    }
    const deleteResponse = await fetch(deleteHatURL, fetchConfig)
    if (deleteResponse.ok){
      const deleted = await deleteResponse.json()
      console.log(deleted)
      window.location.reload(false)
    }
  }
    
  render(){
    return (
      <table className="table table-striped">
        <thead>
          <tr> 
            <th>
              <a href= "http://localhost:3000/hats/new/" role="button" className="btn btn-success">+ Create Hat</a>
            </th>
          </tr>
          <tr>
            <th>Style</th>
            <th>Color</th>
            <th>Fabric</th>
            <th>Location</th>
          </tr>
        </thead>
        <tbody>
          {this.state.hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{ hat.style }</td>
                <td>{ hat.color }</td>
                <td>{ hat.fabric }</td>
                <td>{ hat.location.closet_name }</td>
                <td><img alt=""className="photo" src={hat.picture_url}/> </td>
                <td> <button onClick={() => this.delete(hat.id)} className="btn btn-danger">Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
      
    );
  }
} 
export default HatsList;